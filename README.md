# GD NONG DOWNLOADER
## Overview
This software created for easily replace and offset NONG gd songs.
![Screenshot 0](screenshot.png)
![Screenshot 1](screenshot1.png)
![Screenshot 2](screenshot2.png)
![Screenshot 3](screenshot3.png)
## License
Check out [license](https://codeberg.org/ignitedoreo/gd-nong-downloader/src/branch/master/LICENSE) file.
## Installation
First off, you need python (3.10 tested).

Download (clone) repo, open cmd in folder and do these commands:
```bash
$ pip install -r requirements.txt
$ python updateFfmpegYtdlp.py
# It will download and copy ffmpeg.exe and yt-dlp.exe to current directory
$ python main.py
```
This should run the app.