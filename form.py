# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'form.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(465, 261)
        icon = QIcon()
        icon.addFile(u"GeometryDash.ico", QSize(), QIcon.Normal, QIcon.Off)
        Form.setWindowIcon(icon)
        self.horizontalLayout = QHBoxLayout(Form)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.vboxlayout = QVBoxLayout()
        self.vboxlayout.setObjectName(u"vboxlayout")
        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.radioButton = QRadioButton(Form)
        self.buttonGroup = QButtonGroup(Form)
        self.buttonGroup.setObjectName(u"buttonGroup")
        self.buttonGroup.addButton(self.radioButton)
        self.radioButton.setObjectName(u"radioButton")
        self.radioButton.setChecked(True)

        self.horizontalLayout_3.addWidget(self.radioButton)

        self.radioButton_2 = QRadioButton(Form)
        self.buttonGroup.addButton(self.radioButton_2)
        self.radioButton_2.setObjectName(u"radioButton_2")

        self.horizontalLayout_3.addWidget(self.radioButton_2)

        self.radioButton_5 = QRadioButton(Form)
        self.buttonGroup.addButton(self.radioButton_5)
        self.radioButton_5.setObjectName(u"radioButton_5")

        self.horizontalLayout_3.addWidget(self.radioButton_5)


        self.vboxlayout.addLayout(self.horizontalLayout_3)

        self.stackedWidget = QStackedWidget(Form)
        self.stackedWidget.setObjectName(u"stackedWidget")
        self.stackedWidget.setFrameShape(QFrame.NoFrame)
        self.stackedWidget.setFrameShadow(QFrame.Plain)
        self.page = QWidget()
        self.page.setObjectName(u"page")
        self.horizontalLayout_5 = QHBoxLayout(self.page)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.lineEdit = QLineEdit(self.page)
        self.lineEdit.setObjectName(u"lineEdit")

        self.horizontalLayout_5.addWidget(self.lineEdit)

        self.stackedWidget.addWidget(self.page)
        self.page_2 = QWidget()
        self.page_2.setObjectName(u"page_2")
        self.horizontalLayout_4 = QHBoxLayout(self.page_2)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.lineEdit_2 = QLineEdit(self.page_2)
        self.lineEdit_2.setObjectName(u"lineEdit_2")

        self.horizontalLayout_4.addWidget(self.lineEdit_2)

        self.stackedWidget.addWidget(self.page_2)
        self.page_5 = QWidget()
        self.page_5.setObjectName(u"page_5")
        self.horizontalLayout_2 = QHBoxLayout(self.page_5)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.lineEdit_8 = QLineEdit(self.page_5)
        self.lineEdit_8.setObjectName(u"lineEdit_8")

        self.horizontalLayout_2.addWidget(self.lineEdit_8)

        self.stackedWidget.addWidget(self.page_5)

        self.vboxlayout.addWidget(self.stackedWidget)

        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.radioButton_4 = QRadioButton(Form)
        self.buttonGroup_2 = QButtonGroup(Form)
        self.buttonGroup_2.setObjectName(u"buttonGroup_2")
        self.buttonGroup_2.addButton(self.radioButton_4)
        self.radioButton_4.setObjectName(u"radioButton_4")
        self.radioButton_4.setChecked(True)

        self.horizontalLayout_6.addWidget(self.radioButton_4)

        self.radioButton_3 = QRadioButton(Form)
        self.buttonGroup_2.addButton(self.radioButton_3)
        self.radioButton_3.setObjectName(u"radioButton_3")
        self.radioButton_3.setCheckable(True)

        self.horizontalLayout_6.addWidget(self.radioButton_3)


        self.vboxlayout.addLayout(self.horizontalLayout_6)

        self.stackedWidget_2 = QStackedWidget(Form)
        self.stackedWidget_2.setObjectName(u"stackedWidget_2")
        self.page_3 = QWidget()
        self.page_3.setObjectName(u"page_3")
        self.horizontalLayout_7 = QHBoxLayout(self.page_3)
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.lineEdit_4 = QLineEdit(self.page_3)
        self.lineEdit_4.setObjectName(u"lineEdit_4")

        self.horizontalLayout_7.addWidget(self.lineEdit_4)

        self.stackedWidget_2.addWidget(self.page_3)
        self.page_4 = QWidget()
        self.page_4.setObjectName(u"page_4")
        self.verticalLayout = QVBoxLayout(self.page_4)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.lineEdit_6 = QLineEdit(self.page_4)
        self.lineEdit_6.setObjectName(u"lineEdit_6")

        self.verticalLayout.addWidget(self.lineEdit_6)

        self.lineEdit_5 = QLineEdit(self.page_4)
        self.lineEdit_5.setObjectName(u"lineEdit_5")

        self.verticalLayout.addWidget(self.lineEdit_5)

        self.stackedWidget_2.addWidget(self.page_4)

        self.vboxlayout.addWidget(self.stackedWidget_2)

        self.lineEdit_3 = QLineEdit(Form)
        self.lineEdit_3.setObjectName(u"lineEdit_3")

        self.vboxlayout.addWidget(self.lineEdit_3)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.vboxlayout.addItem(self.verticalSpacer_2)

        self.lineEdit_7 = QLineEdit(Form)
        self.lineEdit_7.setObjectName(u"lineEdit_7")

        self.vboxlayout.addWidget(self.lineEdit_7)

        self.toolButton_2 = QToolButton(Form)
        self.toolButton_2.setObjectName(u"toolButton_2")

        self.vboxlayout.addWidget(self.toolButton_2)


        self.horizontalLayout.addLayout(self.vboxlayout)

        self.vboxlayout_2 = QVBoxLayout()
        self.vboxlayout_2.setObjectName(u"vboxlayout_2")
        self.image_preview = QLabel(Form)
        self.image_preview.setObjectName(u"image_preview")

        self.vboxlayout_2.addWidget(self.image_preview)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.vboxlayout_2.addItem(self.verticalSpacer)

        self.label = QLabel(Form)
        self.label.setObjectName(u"label")

        self.vboxlayout_2.addWidget(self.label)

        self.label_2 = QLabel(Form)
        self.label_2.setObjectName(u"label_2")

        self.vboxlayout_2.addWidget(self.label_2)

        self.toolButton = QToolButton(Form)
        self.toolButton.setObjectName(u"toolButton")
        self.toolButton.setEnabled(False)

        self.vboxlayout_2.addWidget(self.toolButton)


        self.horizontalLayout.addLayout(self.vboxlayout_2)


        self.retranslateUi(Form)

        self.stackedWidget.setCurrentIndex(0)
        self.stackedWidget_2.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"GD NONG song replacer", None))
        self.radioButton.setText(QCoreApplication.translate("Form", u"YouTube Link", None))
        self.radioButton_2.setText(QCoreApplication.translate("Form", u"NG Song", None))
        self.radioButton_5.setText(QCoreApplication.translate("Form", u"Direct Link", None))
        self.lineEdit.setPlaceholderText(QCoreApplication.translate("Form", u"YouTube Link (e.g. https://youtube.com/watch?v=dQw4w9WgXcQ)", None))
        self.lineEdit_2.setPlaceholderText(QCoreApplication.translate("Form", u"NG Song ID (e.g. 930302)", None))
        self.lineEdit_8.setPlaceholderText(QCoreApplication.translate("Form", u"Direct Link (e.g. https://somewebsite.com/music.mp3)", None))
        self.radioButton_4.setText(QCoreApplication.translate("Form", u"Offset Music by Seconds", None))
        self.radioButton_3.setText(QCoreApplication.translate("Form", u"Drop offset calculation", None))
        self.lineEdit_4.setPlaceholderText(QCoreApplication.translate("Form", u"Music offset in seconds (e.g. 10)", None))
        self.lineEdit_6.setPlaceholderText(QCoreApplication.translate("Form", u"Replace song drop in seconds (e.g. 81)", None))
        self.lineEdit_5.setPlaceholderText(QCoreApplication.translate("Form", u"Original song drop in seconds (e.g. 41)", None))
        self.lineEdit_3.setPlaceholderText(QCoreApplication.translate("Form", u"AppData GD Folder (e.g. GeometryDash)", None))
        self.lineEdit_7.setPlaceholderText(QCoreApplication.translate("Form", u"ID to replace (e.g. 930302)", None))
        self.toolButton_2.setText(QCoreApplication.translate("Form", u"Fetch Info", None))
        self.image_preview.setText("")
        self.label.setText(QCoreApplication.translate("Form", u"GD NONG Downloader", None))
        self.label_2.setText(QCoreApplication.translate("Form", u"by ignitedoreo/cosmo", None))
        self.toolButton.setText(QCoreApplication.translate("Form", u"Replace", None))
    # retranslateUi

