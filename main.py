from PySide2 import (
    QtWidgets,
    QtCore,
    QtGui
)
from form import Ui_Form
from PyGrounds import Song
import sys
import requests
import pytube as pt
import re
import os
import subprocess

app = QtWidgets.QApplication(sys.argv)
form = QtWidgets.QWidget()
ui = Ui_Form()
ui.setupUi(form)
form.show()

data = open("assets/default.png", "rb").read()
pixmap = QtGui.QPixmap()
pixmap.loadFromData(QtCore.QByteArray(data))
pixmap = pixmap.scaled(240, 160)
ui.image_preview.setPixmap(pixmap)

def downloadModeChanged():
    if ui.radioButton.isChecked():
        ui.stackedWidget.setCurrentIndex(0)
    if ui.radioButton_2.isChecked():
        ui.stackedWidget.setCurrentIndex(1)
    if ui.radioButton_5.isChecked():
        ui.stackedWidget.setCurrentIndex(2)

def offsetModeChanged():
    if ui.radioButton_3.isChecked():
        ui.stackedWidget_2.setCurrentIndex(1)
    if ui.radioButton_4.isChecked():
        ui.stackedWidget_2.setCurrentIndex(0)

def fetchSongInfo():
    ui.toolButton.setEnabled(True)
    if ui.radioButton.isChecked():
        if re.match("http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?", ui.lineEdit.text()):
            video = pt.YouTube(ui.lineEdit.text())
            ui.label.setText(video.title)
            ui.label_2.setText(f"by {video.author}")
            data = requests.get(video.thumbnail_url).content
            pixmap = QtGui.QPixmap()
            pixmap.loadFromData(QtCore.QByteArray(data))
            pixmap = pixmap.scaled(240, 160)
            ui.image_preview.setPixmap(pixmap)
    if ui.radioButton_2.isChecked():
        song = Song(int(ui.lineEdit_2.text()))
        ui.label.setText(song.name)
        ui.label_2.setText(f"by {song.author}")
        data = open("assets/ng_default.png", "rb").read()
        pixmap = QtGui.QPixmap()
        pixmap.loadFromData(QtCore.QByteArray(data))
        pixmap = pixmap.scaled(240, 160)
        ui.image_preview.setPixmap(pixmap)
    if ui.radioButton_5.isChecked():
        ui.label.setText(f"External MP3")
        ui.label_2.setText(f"")
        data = open("assets/mp3_default.png", "rb").read()
        pixmap = QtGui.QPixmap()
        pixmap.loadFromData(QtCore.QByteArray(data))
        pixmap = pixmap.scaled(240, 160)
        ui.image_preview.setPixmap(pixmap)

def download():
    if ui.lineEdit_3.text() == "" or ui.lineEdit_7.text() == "":
        return
    app.processEvents()
    if os.path.isfile(f"{os.getenv('localappdata')}\\{ui.lineEdit_3.text()}\\temp.mp3"):
        print(" - temp.mp3 exists, deleting it...")
        subprocess.run(f"del {os.getenv('localappdata')}\\{ui.lineEdit_3.text()}\\temp.mp3", shell=True)
    if ui.radioButton.isChecked():
        print(" - checking youtube link to match regexp...")
        if re.match("http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?", ui.lineEdit.text()):
            print(" - matches! downloading video as mp3...")
            subprocess.run(f"yt-dlp --extract-audio --audio-quality 320k --audio-format mp3 --output {os.getenv('localappdata')}/{ui.lineEdit_3.text()}/temp.mp3 {ui.lineEdit.text()}", shell=True)
        else:
            print(" - not matches! returning...")
            return
    if ui.radioButton_2.isChecked():
        print(" - downloading song from ng...")
        song = Song(int(ui.lineEdit_2.text()))
        song.download(f"{os.getenv('localappdata')}/{ui.lineEdit_3.text()}/temp.mp3")
    if ui.radioButton_5.isChecked():
        print(" - downloading song from direct link...")
        song = requests.get(ui.lineEdit_8.text()).content
        with open(f"{os.getenv('localappdata')}/{ui.lineEdit_3.text()}/temp.mp3", "wb") as file:
            file.write(song)
    print(" - offsetting music...")
    if ui.radioButton_4.isChecked():
        print(" - offsetting by seconds...")
        if int(ui.lineEdit_4.text()) > 0:
            print(" - more than zero, doing some job...")
            subprocess.run(f"ffmpeg -ss {ui.lineEdit_4.text()} -i {os.getenv('localappdata')}\\{ui.lineEdit_3.text()}\\temp.mp3 -y {os.getenv('localappdata')}\\{ui.lineEdit_3.text()}\\{ui.lineEdit_7.text()}.mp3", shell=True)
        elif int(ui.lineEdit_4.text()) < 0:
            print(" - less than zero, doing some job...")
            subprocess.run(f"ffmpeg -f lavfi -t {-int(ui.lineEdit_4.text())} -i anullsrc=channel_layout=stereo:sample_rate=44100 -i {os.getenv('localappdata')}\\{ui.lineEdit_3.text()}\\temp.mp3 -filter_complex \"[0:a][1:a]concat=n=2:v=0:a=1\" -y {os.getenv('localappdata')}\\{ui.lineEdit_3.text()}\\{ui.lineEdit_7.text()}.mp3", shell=True)
        else:
            print(" - zero! doing nothing.")
            subprocess.run(f"xcopy {os.getenv('localappdata')}\\{ui.lineEdit_3.text()}\\temp.mp3 {os.getenv('localappdata')}\\{ui.lineEdit_3.text()}\\{ui.lineEdit_7.text()}.mp3 /Y", shell=True)
    else:
        print(" - offsetting by drop...")
        if int(ui.lineEdit_6.text()) - int(ui.lineEdit_5.text()) > 0:
            print(" - more than zero, doing some job...")
            subprocess.run(f"ffmpeg -ss {int(ui.lineEdit_6.text()) - int(ui.lineEdit_5.text())} -i {os.getenv('localappdata')}\\{ui.lineEdit_3.text()}\\temp.mp3 -y {os.getenv('localappdata')}\\{ui.lineEdit_3.text()}\\{ui.lineEdit_7.text()}.mp3", shell=True)
        elif int(ui.lineEdit_6.text()) - int(ui.lineEdit_5.text()) < 0:
            print(" - less than zero, doing some job...")
            subprocess.run(f"ffmpeg -f lavfi -t {-(int(ui.lineEdit_6.text()) - int(ui.lineEdit_5.text()))} -i anullsrc=channel_layout=stereo:sample_rate=44100 -i {os.getenv('localappdata')}\\{ui.lineEdit_3.text()}\\temp.mp3 -filter_complex \"[0:a][1:a]concat=n=2:v=0:a=1\" -y {os.getenv('localappdata')}\\{ui.lineEdit_3.text()}\\{ui.lineEdit_7.text()}.mp3", shell=True)
        else:
            print(" - zero! doing nothing.")
            subprocess.run(f"xcopy {os.getenv('localappdata')}\\{ui.lineEdit_3.text()}\\temp.mp3 {os.getenv('localappdata')}\\{ui.lineEdit_3.text()}\\{ui.lineEdit_7.text()}.mp3 /Y", shell=True)
    print(" - done!")

ui.radioButton.clicked.connect(downloadModeChanged)
ui.radioButton_2.clicked.connect(downloadModeChanged)
ui.radioButton_5.clicked.connect(downloadModeChanged)

ui.radioButton_3.clicked.connect(offsetModeChanged)
ui.radioButton_4.clicked.connect(offsetModeChanged)

ui.toolButton_2.clicked.connect(fetchSongInfo)
ui.toolButton.clicked.connect(download)

sys.exit(app.exec_())
